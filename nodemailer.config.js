/*
* Nodemailer configuration
* For supported services, see https://github.com/andris9/nodemailer-wellknown#supported-services
*/
// For todays date;
Date.prototype.today = function () {
    return ((this.getDate() < 10)?"0":"") + this.getDate() +"/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ this.getFullYear();
}

// For the time now
Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}

module.exports = function() {
  var datetime = new Date().today() + " @ " + new Date().timeNow();

  var config = {
    transportOptions: {
      host: 'mail.dreamsmiths-developers.co.za',
      port: 465,
      secure: true, // upgrade later with STARTTLS
      auth: {
          user: 'mailtest@dreamsmiths-developers.co.za',
          pass: '!MAILERStest1024#'
      },
      tls: {
          // do not fail on invalid certs
          rejectUnauthorized: false
      }
    },
    mailOptions: {
      to: 'gerhardt.vanrooyen@dreamsmiths.com,gerhardtvanrooyen@gmail.com,danielle.lazarus@dreamsmiths.com,yusrah.solomons@dreamsmiths.com,ds_test01@hotmail.com,ds_test01@yahoo.com,dsmithstest01@gmail.com', // Default address(es) to send test emails to (can be comma separated)
      from: 'Cerberus <mailtest@dreamsmiths-developers.co.za>', // Sender details
      subject: 'Test summoned by Cerberus - ' + datetime // Default subject line
    },
    imageHost: 'http://dreamsmiths-developers.co.za', // Full url to your image host. Must include http://
    imagePath: '/emails/tests/' // Path to images on server. Must have a slash at the beginning and end.
  };
  return config;
}
