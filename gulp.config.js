/*
* All gulpfile configuration options
*/

var nodemailerConfig = require('./nodemailer.config')();

module.exports = function() {
  var sourceDir = 'source';
  var localDir = 'dist/local';
  var productionDir = 'dist/production';

  var config = {
    localDir: localDir,
    productionDir: productionDir,
    sourceDir: sourceDir,
    localFiles: [
      localDir + '/css/*.css',
      localDir + '/images/**/*',
      localDir + '/*.html'
    ],
    sourcePath: {
      sass: sourceDir + '/stylesheets/**/*.scss',
      html: sourceDir + '/**/*.html',
      layouts: sourceDir + '/layouts/*.html',
      images: sourceDir + '/images/**/*'
    },
    browsersync: {
      port: 8080,
      open: true,
      notify: true
    },
    ftp: {
        host: '41.203.18.10',
        user: 'dreamhzjwr',
        password: 'tfWzvODmHi5',
        remote: '/public_html'
    },
    nodemailer: nodemailerConfig
  };
  return config;
}
